*This is italic text.*

This is `a link`_.

*This is also italic text, with `a link`_ in it.*

.. _a link: https://gitlab.com